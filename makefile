# ============= compiler settings ==============================================
CC       ?= gcc
CFLAGS    = -std=c99 -g3 -Os -Wall -Wextra -Werror -march=native
CFLAGS   += -D_POSIX_C_SOURCE
LDFLAGS   =
# ==============================================================================

# ==============================================================================
# DEBUG malloc
#
# LDFLAGS += -Wl,--wrap,malloc -Wl,--wrap,realloc -Wl,--wrap,calloc -Wl,--wrap,free
# ==============================================================================

# ============= common library sources =========================================
LIB_SRCS  =
LIB_OBJS  = $(addprefix obj/,$(LIB_SRCS:.c=.o))
# ==============================================================================

# ============= executable sources =============================================
EXE_SRCS  = $(filter-out $(LIB_SRCS), $(wildcard src/*.c))
EXE_OBJS  = $(addprefix obj/, $(EXE_SRCS:.c=.o))
EXE_BINS  = $(addprefix bin/, $(EXE_SRCS:.c=))
# ==============================================================================

# ============= dependencies ===================================================
DEPENDS   = $(EXE_OBJS:.o=.d) $(LIB_OBJS:.o=.d)
# ==============================================================================

all: $(EXE_BINS) test

test: ./bin/src/test-unit-test
	@./bin/src/test-unit-test

-include $(DEPENDS)

obj/%.o: %.c
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -MMD -MP -MF $(@:%.o=%.d) -c -o $@ $<

bin/%: obj/%.o $(LIB_OBJS)
	@mkdir -p $(dir $@)
	$(CC) $(LDFLAGS) -Wl,-Map=$@.map -o $@ $^

clean:
	rm -rf bin/ obj/

# allows make print-<x> to print the value of variable x
print-%:
	@echo
	@echo $*:
	@echo $($*)
	@echo

.PHONY: all clean test
.SECONDARY: $(LIB_OBJS) $(EXE_OBJS)

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-20s\033[0m %s\n", gensub(/^.*:/, "", "g", $$1), $$2}'

