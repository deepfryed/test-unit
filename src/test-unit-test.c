#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stddef.h>
#include <sys/types.h>

#include "test-unit.h"

TEST_UNIT_TEST(hello_world_1, "test", "check 1s") {
  test_unit_assert(1 == 1, "1 == 1");
  return true;
}

TEST_UNIT_TEST(hello_world_2, "test", "check 2s") {
  test_unit_assert(1 == 2, "1 == 2");
  return true;
}

TEST_UNIT_TEST(hello_world_3, "test", "check 3s") {
  test_unit_assert(1 == 3, "1 == 3");
  return true;
}

int main () {
  int passed = 0, failed = 0;
  TEST_UNIT_RUN_TESTS(passed, failed);

  if (failed > 0)
    fprintf(stderr, "\n%s[PASS]%s test suite\n", TEST_UNIT_ANSI_COLOR_GREEN, TEST_UNIT_ANSI_COLOR_RESET);
  else
    exit(1);
}
