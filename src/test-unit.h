#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stddef.h>
#include <unistd.h>

#ifndef TEST_UNIT_LABEL_SIZE
  #define TEST_UNIT_LABEL_SIZE (128)
#endif

typedef bool (*test_unit_test_fn_t)(void);

#define TEST_UNIT_ANSI_COLOR_GREEN   (isatty(fileno(stdout)) ? "\033[32;1m"   : "")
#define TEST_UNIT_ANSI_COLOR_RED     (isatty(fileno(stdout)) ? "\033[31;1m"   : "")
#define TEST_UNIT_ANSI_COLOR_CYAN    (isatty(fileno(stdout)) ? "\033[36;1;3m" : "")
#define TEST_UNIT_ANSI_COLOR_RESET   (isatty(fileno(stdout)) ? "\033[0m"      : "")

#define test_unit_test_pass(format, ...) \
  do { \
    fprintf(stderr, "    %s✔%s "format "\n", \
      TEST_UNIT_ANSI_COLOR_GREEN, TEST_UNIT_ANSI_COLOR_RESET, ##__VA_ARGS__); \
  } while (0)

#define test_unit_test_fail(format, ...) \
  do { \
    fprintf(stderr, "    %s✘%s "format "\n", \
      TEST_UNIT_ANSI_COLOR_RED, TEST_UNIT_ANSI_COLOR_RESET, ##__VA_ARGS__); \
    return false; \
  } while (0)

#define test_unit_assert(test, format, ...) \
  do { \
    if ((test)) \
      test_unit_test_pass(format, ##__VA_ARGS__); \
    else \
      test_unit_test_fail(format, ##__VA_ARGS__); \
  } while (0)

#ifdef __x86_64__
  #define TEST_UNIT_ALIGNMENT 8
#else
  #define TEST_UNIT_ALIGNMENT 4
#endif

#pragma pack(push, TEST_UNIT_ALIGNMENT)
typedef struct {
  test_unit_test_fn_t test_fn;
  char scenario[TEST_UNIT_LABEL_SIZE];
  char name[TEST_UNIT_LABEL_SIZE];
} test_unit_test_t;
#pragma pack(pop)

#define TEST_UNIT_PUSH_TEST(_t, _s, _n)                \
  static test_unit_test_t ptr_##_t                     \
  __attribute((used, section("test_unit_tests")))      \
  __attribute((aligned(TEST_UNIT_ALIGNMENT)))          \
  = {                                                  \
    .test_fn = _t,                                     \
    .scenario = _s,                                    \
    .name = _n                                         \
  };

#define TEST_UNIT_RUN_TESTS(_passed, _failed)                \
  do {                                                       \
    char scenario[TEST_UNIT_LABEL_SIZE]= "";                 \
    extern test_unit_test_t __start_test_unit_tests;         \
    extern test_unit_test_t __stop_test_unit_tests;          \
    for (                                                    \
      test_unit_test_t *test = &__start_test_unit_tests;     \
      test != &__stop_test_unit_tests;                       \
      ++test                                                 \
     ) {                                                     \
        if (strcmp(scenario, test->scenario) != 0) {         \
          fprintf(stderr, "\n%sScenario%s: %s\n\n",          \
            TEST_UNIT_ANSI_COLOR_CYAN,                       \
            TEST_UNIT_ANSI_COLOR_RESET, test->scenario);     \
          strcpy(scenario, test->scenario);                  \
        }                                                    \
        fprintf(stderr, "  %s\n", test->name);               \
        if (test->test_fn())                                 \
          _passed++;                                         \
        else                                                 \
          _failed++;                                         \
        fprintf(stderr, "\n");                               \
    }                                                        \
                                                             \
    if (_failed > 0) {                                       \
      fprintf(stderr, "\n%s[FAIL]%s %d passed %d failed\n",  \
        TEST_UNIT_ANSI_COLOR_RED,                            \
        TEST_UNIT_ANSI_COLOR_RESET, _passed, _failed);       \
    }                                                        \
    else {                                                   \
      fprintf(stderr, "\n%s[PASS]%s %d passed %d failed\n",  \
        TEST_UNIT_ANSI_COLOR_GREEN,                          \
        TEST_UNIT_ANSI_COLOR_RESET, _passed, _failed);       \
    }                                                        \
  } while (0)

#define TEST_UNIT_TEST(id, scenario, name)                   \
  static bool _test_unit_##id(void);                         \
  TEST_UNIT_PUSH_TEST(_test_unit_##id, scenario, name)       \
  static bool _test_unit_##id(void)

#ifdef __cplusplus
}
#endif
