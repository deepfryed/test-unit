# Simple unit test runner for C

## Example

### Source

```c
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stddef.h>
#include <sys/types.h>

#include "test-unit.h"

TEST_UNIT_TEST(hello_world_1, "test", "check 1s") {
  test_unit_assert(1 == 1, "1 == 1");
  return true;
}

TEST_UNIT_TEST(hello_world_2, "test", "check 2s") {
  test_unit_assert(1 == 2, "1 == 2");
  return true;
}

TEST_UNIT_TEST(hello_world_3, "test", "check 3s") {
  test_unit_assert(1 == 3, "1 == 3");
  return true;
}

int main () {
  TEST_UNIT_RUN_TESTS();
}
```

### Output

```
Scenario: test

  check 3s
    ✘ 1 == 3

  check 2s
    ✘ 1 == 2

  check 1s
    ✔ 1 == 1


[FAIL] 1 passed 2 failed

```

## License

BSD 3-Clause
